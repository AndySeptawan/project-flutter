import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(
    MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.lightBlueAccent,
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.web,
              color: Colors.white,
            ),
          ),
          title: Text("Aplikasi Andy Septawan"),
          backgroundColor: Colors.blue,
          actions: [
            IconButton(
              icon: Icon(Icons.thumb_up, color: Colors.white),
            ),
            IconButton(
              icon: Icon(Icons.thumb_down, color: Colors.white),
            ),
          ],
        ),
        body: Column(
          children: <Widget>[
            Flexible(
              flex: 2,
              child: Container(
                height: 400,
                margin: EdgeInsets.all(5),
                decoration: BoxDecoration(
                    color: Colors.blue[200],
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                        image: NetworkImage(
                            'https://mahasiswa.undiksha.ac.id/media/photo/1ac3226cce0a49800ac76b5fd47313ce20180722010722.jpg'),
                        fit: BoxFit.fill)),
              ),
            ),
            Text('I Ketut Andy Septawan',
                style: TextStyle(
                    fontFamily: 'Rifia', fontSize: 24, color: Colors.red)),
          ],
        ),
      ),
    ),
  );
}
